import moment from 'moment';

exports.convertTimeToReadableFormat = number => {
  let hours = Math.floor(number / 60);
  let minutes = number % 60;
  let period = 'AM';
  if (hours >= 12) {
    if (hours > 12) {
      hours = hours - 12;
    }
    period = 'PM';
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  return `${hours}:${minutes} ${period}`;
};

exports.convertDateToReadableFormat = dateText => {
  try {
    dateText = dateText.substr(0, dateText.indexOf('T'));
    let dateString =
      moment(dateText).date() +
      ' ' +
      moment(moment(dateText).month() + 1, 'M').format('MMMM') +
      ' ' +
      moment(dateText).year();
    return dateString;
  } catch (error) {
    return dateText;
  }
};

exports.capitaliseFirstLetter = string => {
  if (!string) return string;
  return string.charAt(0).toUpperCase() + string.slice(1);
};

exports.getWorkingDays = list => {
  const workingDays = {
    1: 'Monday',
    2: 'Tuesday',
    3: 'Wednesday',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday',
    7: 'Sunday',
  };
  if (!list) return '';
  const newList = list.map(Math.floor);
  newList.sort((a, b) => a - b);
  let workingDaysString = '';
  newList.forEach(element => {
    workingDaysString += workingDays[element];
    workingDaysString += ' ';
  });
  //   console.log(workingDaysString);
  return workingDaysString;
};

exports.checkIfWorkingDay = (workingDays, selectedDate) => {
  let workingList = workingDays.map(Math.floor);
  // console.log('workingList:', workingList);
  workingList = workingList.map(l => (l === 7 ? 0 : l));
  // console.log('workingList:', workingList);
  selectedDay = moment(selectedDate, 'MM-DD-YYYY').day();
  // console.log('selectedDay:', selectedDay);
  isWorkingDay = workingList.includes(selectedDay);
  //   console.log('IsWorkingDay:', isWorkingDay);
  return isWorkingDay;
};

exports.validateEmail = email => {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
};

exports.validateName = name => {
  if (name.length < 3) return false;
  return true;
};

exports.validatePassword = password => {
  if (password.length < 6) return false;
  return true;
};
