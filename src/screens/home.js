import * as React from 'react';
// import {Text, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Sellers from './sellers';
import Settings from './settings';
import Appointments from './appointments';

const Tab = createBottomTabNavigator();

export default function Home() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Sellers') {
            iconName = 'ios-briefcase';
          } else if (route.name === 'Settings') {
            iconName = 'ios-cog';
          } else if (route.name === 'Appointments') {
            iconName = focused
              ? 'ios-checkmark-circle'
              : 'ios-checkmark-circle-outline';
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen name="Sellers" component={Sellers} />
      <Tab.Screen name="Appointments" component={Appointments} />
      <Tab.Screen name="Settings" component={Settings} />
    </Tab.Navigator>
  );
}
