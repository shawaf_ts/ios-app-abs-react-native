import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {validateEmail, validatePassword, validateName} from '../helpers/util';
import {
  View,
  Text,
  FlatList,
  ScrollView,
  StyleSheet,
  Alert,
} from 'react-native';
import {Input, Button} from 'react-native-elements';
import {signup} from '../services/authService';

class Signup extends Component {
  state = {
    name: '',
    email: '',
    password: '',
  };

  onChangeName = value => {
    const name = value;
    this.setState({name});
  };
  onChangeEmail = value => {
    const email = value;
    this.setState({email});
  };
  onChangePassword = value => {
    const password = value;
    this.setState({password});
  };

  onSignup = async () => {
    const {email, password, name} = this.state;
    if (
      !validateEmail(email) ||
      !validatePassword(password) ||
      !validateName(name)
    ) {
      Alert.alert(
        'Invalid Input',
        'Incorrect email/password\n Password must a minimum of 6 characters \n Name should be atleast 3 characters',
      );
      return;
    }

    try {
      await signup(email.toLowerCase(), password, name.toLowerCase());
    } catch (error) {
      console.log('FROM SIGNUP Error.response: ', error);
      if (error.response.status === 400) {
        Alert.alert('User already registered', 'Please try logging in');
        return;
      } else {
        console.log('Signup error:', error);
        Alert.alert(
          'Network Error',
          'Please check the internet connection and try again',
        );
        return;
      }
    }
    this.props.navigation.navigate('Home');
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <ScrollView keyboardShouldPersistTaps="never">
          <Input
            value={this.state.name}
            label="Your Name"
            placeholder="Full Name"
            onChangeText={this.onChangeName}
            leftIcon={{type: 'font-awesome', name: 'envelope'}}
            containerStyle={styles.input}
            inputStyle={{paddingLeft: 10}}
          />
          <Input
            value={this.state.email}
            label="Your Email Address"
            placeholder="email@address.com"
            onChangeText={this.onChangeEmail}
            leftIcon={{type: 'font-awesome', name: 'envelope'}}
            containerStyle={styles.input}
            inputStyle={{paddingLeft: 10}}
          />
          <Input
            value={this.state.password}
            label="Password"
            placeholder="Enter Password"
            onChangeText={this.onChangePassword}
            containerStyle={styles.input}
            inputStyle={{paddingLeft: 10}}
            secureTextEntry
            leftIcon={{type: 'font-awesome', name: 'envelope'}}
          />

          <Button
            title="Sign Up"
            onPress={this.onSignup}
            style={{
              paddingTop: 20,
              width: '30%',
              alignSelf: 'center',
              flex: 1,
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'center',
    paddingTop: 40,
    padding: 20,
  },
  input: {
    padding: '5%',
    // margin: '10%',
  },
});

export default Signup;
