import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Keyboard,
  Alert,
} from 'react-native';
import {getSellers} from '../services/sellerService';
import {SearchBar} from 'react-native-elements';
import CustomCards from '../components/customCards';

import {checkLoggedIn} from '../services/authService';

class Sellers extends Component {
  state = {
    searchQuery: '',
    sellers: [],
  };

  async componentDidMount() {
    try {
      const logged = await checkLoggedIn();
      if (!logged) this.props.navigation.navigate('Login');
      const {data: sellers} = await getSellers();
      this.setState({sellers});

      this.props.navigation.addListener('focus', async () => {
        const {data: sellers} = await getSellers();
        this.setState({sellers});
        console.log('Appointments: Add Listener:', sellers);
      });
    } catch (error) {
      console.log('Error:', error);
      Alert.alert(
        'Network Error',
        'Please check the internet connection and try again',
      );
    }
  }

  updateSearch = searchQuery => {
    this.setState({searchQuery: searchQuery});
  };

  clearSearch = () => {
    const search = '';
    this.setState({search});
    Keyboard.dismiss();
  };

  filterSeller = () => {
    const {searchQuery, sellers} = this.state;
    let filtered = sellers;
    if (searchQuery) {
      filtered = sellers.filter(m =>
        m.name.toLowerCase().startsWith(searchQuery.toLowerCase()),
      );
    }
    return filtered;
  };

  render() {
    const {searchQuery} = this.state;
    const filteredData = this.filterSeller();
    return (
      <View>
        <ScrollView>
          <SearchBar
            placeholder="Search Sellers..."
            onChangeText={this.updateSearch}
            value={searchQuery}
            onClear={this.clearSearch}
            inputContainerStyle={{backgroundColor: '#ffffff'}}
            inputStyle={{color: 'black'}}
          />
          <CustomCards
            sellers={filteredData}
            navigation={this.props.navigation}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  helloText: {
    fontSize: 40,
  },
});

export default Sellers;
