import React, {Component} from 'react';
import moment from 'moment';
import {View, SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import CustomDatePicker from '../components/common/customDatePicker';
import {Button} from 'react-native-elements';
import TimeSlotPicker from '../components/common/timeSlotsPicker';
import {getTimeSlots, bookAppointment} from '../services/buyerService';
import {getSeller} from '../services/sellerService';
import {getCurrentUser} from '../services/authService';
import InfoCard from '../components/common/infoCard';

class Booking extends Component {
  state = {
    buyerId: '',
    sellerId: '',
    sellerInfo: {},
    timeSlots: [],
    date: moment()
      .add(1, 'd')
      .format('MM-DD-YYYY'),
    selectedSlot: '',
  };

  async componentDidMount() {
    try {
      const {_id: buyerId} = await getCurrentUser();
      // console.log('buyerId:', buyerId);
      const {sellerId} = this.props.route.params;
      // console.log('Seller id from params', sellerId);
      const {data: timeSlots} = await getTimeSlots(sellerId, this.state.date);
      const {data: sellerInfo} = await getSeller(sellerId);
      // console.log('Seller Info:', sellerInfo);
      this.setState({timeSlots, sellerInfo, sellerId, buyerId});
    } catch (error) {
      console.log('Booking.js: Error:', error);
    }
  }

  onDateChange = async date => {
    const {data: timeSlots} = await getTimeSlots(this.state.sellerId, date);
    this.setState({timeSlots, date});
  };

  onSlotSelect = slot => {
    this.setState({selectedSlot: slot});
  };

  onBook = async () => {
    try {
      const res = await bookAppointment(
        this.state.sellerId,
        this.state.buyerId,
        this.state.date,
        this.state.selectedSlot,
      );
      this.setState({selectedSlot: ''});
      const {data: timeSlots} = await getTimeSlots(
        this.state.sellerId,
        this.state.date,
      );
      this.setState({timeSlots});
      this.props.navigation.navigate('Home', {
        screen: 'Appointments',
        params: res.data.id,
      });
    } catch (error) {}
  };

  render() {
    // console.log(this.state.date);

    // console.log('Selected Slot:', this.state.selectedSlot);
    // console.log('Seller Info:', this.state.sellerInfo);
    return (
      <SafeAreaView>
        <View style={styles.mainContainer}>
          <ScrollView>
            <InfoCard sellerInfo={this.state.sellerInfo} />
            <CustomDatePicker
              date={this.state.date}
              onDateChange={this.onDateChange}
            />
            <TimeSlotPicker
              selectedDate={this.state.date}
              workingDays={this.state.sellerInfo.workingDays}
              selectedSlot={this.state.selectedSlot}
              timeSlots={this.state.timeSlots}
              onSlotSelect={this.onSlotSelect}
            />
          </ScrollView>

          <Button
            title="Book Appointment"
            disabled={!this.state.selectedSlot}
            onPress={this.onBook}
            style={styles.submitButton}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'center',
    paddingTop: 20,
  },
  submitButton: {
    paddingHorizontal: 10,
    position: 'relative',
    bottom: 0,
    left: 0,
  },
});

export default Booking;
