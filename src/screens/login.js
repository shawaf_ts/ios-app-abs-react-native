import React, {Component} from 'react';
import 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import {View, ScrollView, StyleSheet, Keyboard, Alert} from 'react-native';
import {Input, Button} from 'react-native-elements';
import {validateEmail, validatePassword} from '../helpers/util';
import {login, checkLoggedIn} from '../services/authService';

class Login extends Component {
  state = {
    email: '',
    password: '',
  };
  async componentDidMount() {
    const loggedIn = await checkLoggedIn();
    if (loggedIn) {
      this.props.navigation.navigate('Home');
    }
  }
  onChangeEmail = value => {
    const email = value;
    this.setState({email});
  };

  onChangePassword = value => {
    const password = value;
    this.setState({password});
  };
  onSignup = () => {
    this.props.navigation.navigate('Signup');
  };

  onLogin = async () => {
    const {email, password} = this.state;
    if (!validateEmail(email) || !validatePassword(password)) {
      Alert.alert(
        'Invalid Input',
        'Incorrect email/password\n Password must a minimum of 6 characters',
      );
      return;
    }

    try {
      await login(email.toLowerCase(), password);
    } catch (error) {
      if (error.response.status === 400) {
        Alert.alert('Invalid Credentials', 'Email/Password is incorrect');
        return;
      } else {
        Alert.alert(
          'Network Error',
          'Please check the internet connection and try again',
        );
        return;
      }
    }
    this.props.navigation.navigate('Home');
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <ScrollView keyboardShouldPersistTaps="never">
          <Input
            value={this.state.email}
            label="Your Email Address"
            placeholder="email@address.com"
            onChangeText={this.onChangeEmail}
            leftIcon={{type: 'font-awesome', name: 'envelope'}}
            containerStyle={styles.input}
            inputStyle={{paddingLeft: 10}}
          />
          <Input
            value={this.state.password}
            label="Password"
            placeholder="Enter Password"
            onChangeText={this.onChangePassword}
            containerStyle={styles.input}
            inputStyle={{paddingLeft: 10}}
            secureTextEntry
            leftIcon={{type: 'font-awesome', name: 'envelope'}}
          />
          <Button
            onPress={this.onLogin}
            icon={
              <Icon
                name="lock"
                size={20}
                color="white"
                style={{alignItems: 'baseline', left: -10}}
              />
            }
            title="Login"
            style={{
              paddingTop: 20,
              width: '30%',
              alignSelf: 'center',
              flex: 1,
            }}
          />
          <Button
            title="Sign Up"
            onPress={this.onSignup}
            style={{
              paddingTop: 20,
              width: '30%',
              alignSelf: 'center',
              flex: 1,
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'center',
    paddingTop: 40,
    padding: 20,
  },
  input: {
    padding: '5%',
    // margin: '10%',
  },
});
export default Login;
