import React from 'react';
import {View, Text} from 'react-native';
import {Button} from 'react-native-elements';
import {logout} from '../services/authService';

const onLogout = async navigate => {
  try {
    await logout();
  } catch (error) {}
  navigate('Login');
};
const Settings = props => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Button
        title="Logout"
        onPress={() => {
          onLogout(props.navigation.navigate);
        }}></Button>
    </View>
  );
};

export default Settings;
