// import React, {Component, useEffect} from 'react';
import React, {Component} from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import {Card} from '@paraboly/react-native-card';
import {
  convertDateToReadableFormat,
  convertTimeToReadableFormat,
  capitaliseFirstLetter,
} from '../helpers/util';
import {getCurrentUser} from '../services/authService';
import {getAppointments} from '../services/buyerService';
import {useFocusEffect} from '@react-navigation/native';

class Appoinments extends Component {
  state = {appointments: [], buyerId: ''};

  async componentDidMount() {
    const {_id: buyerId} = await getCurrentUser();
    const {data: appointments} = await getAppointments(buyerId);
    // console.log(appointments);
    this.setState({appointments, buyerId});
    this.props.navigation.addListener('focus', async () => {
      const {data: appointments} = await getAppointments(this.state.buyerId);
      // console.log('Appointments: Add Listener:', appointments);
      this.setState({appointments});
    });
  }

  render() {
    // console.log('In Render, Appointments:', this.state.appointments);
    // console.log('BuyerId:', this.state.buyerId);
    return (
      <ScrollView>
        {0 !== this.state.appointments.length && this.state.appointments ? (
          this.state.appointments.map(app => {
            return (
              <View style={styles.cardView} key={app._id}>
                <Card
                  iconDisable={false}
                  iconName="ios-calendar"
                  iconBackgroundColor={getIconBackgroundColor(app)}
                  iconColor="black"
                  iconType="Ionicons"
                  title={capitaliseFirstLetter(app.seller.name)}
                  // key={id}
                  bottomRightText={app.statusOfTheBooking}
                  bottomRightFontSize={14}
                  content={convertDateToReadableFormat(app.appointmentDate)}
                  topRightText={convertTimeToReadableFormat(
                    app.appointmentStartTime,
                  )}
                  titleStyle={{alignItems: 'center', fontSize: 25}}
                />
              </View>
            );
          })
        ) : (
          <View style={{margin: 15}}>
            <Card iconDisable title="No Appointments" />
          </View>
        )}
      </ScrollView>
    );
  }
}

// GREEN - #01DF01
// RED - #FF0000
// YELLOW - #D7DF01

const getIconBackgroundColor = appointment => {
  if (appointment.statusOfTheBooking === 'PENDING') {
    return '#D7DF01';
  } else if (appointment.statusOfTheBooking === 'ACCEPTED') {
    return '#01DF01';
  } else {
    return '#FF0000';
  }
};

const styles = StyleSheet.create({
  cardContainer: {alignItems: 'center', paddingTop: 15},
  cardView: {
    paddingVertical: 10,
    alignItems: 'center',
  },
});

export default Appoinments;
