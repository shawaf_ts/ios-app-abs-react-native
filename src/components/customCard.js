import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Card} from '@paraboly/react-native-card';

const CustomCard = ({id, title, place, onCardPress}) => {
  console.log('Inside Custom Card, props:', id, title);
  return (
    <View style={styles.cardView}>
      <Card
        iconDisable={false}
        iconName="briefcase"
        iconBackgroundColor="#0174DF" //GREY - #A4A4A4
        iconColor="black"
        iconType="Entypo"
        title={title}
        key={id}
        bottomRightText={place}
        bottomRightFontSize={14}
        titleStyle={{alignItems: 'center', fontSize: 25}}
        onPress={() => {
          onCardPress(id);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {alignItems: 'center'},
  cardView: {
    margin: 10,
  },
});

export default CustomCard;
