import React from 'react';
import {Button} from 'react-native-elements';
import {Text, View, StyleSheet} from 'react-native';
import {
  convertTimeToReadableFormat,
  checkIfWorkingDay,
} from '../../helpers/util';

const TimeSlotPicker = ({
  timeSlots,
  onSlotSelect,
  selectedSlot,
  workingDays,
  selectedDate,
}) => {
  let isWorkingDay = true;
  if (timeSlots.length) {
    isWorkingDay = checkIfWorkingDay(workingDays, selectedDate);
    console.log(isWorkingDay);
  }
  return (
    <View style={styles.mainBoxStyle}>
      <Text style={{paddingLeft: 10}}>Select Appointment Time Slot:</Text>
      <View style={styles.timeParentBoxStyle}>
        {0 !== timeSlots.length && isWorkingDay ? (
          timeSlots.map(timeSlot => {
            let time = convertTimeToReadableFormat(
              timeSlot.appointmentStartTime,
            );
            return (
              <View key={timeSlot.appointmentStartTime}>
                <Button
                  buttonStyle={getButtonStyle(timeSlot, selectedSlot)}
                  title={time}
                  titleStyle={styles.titleStyle}
                  key={timeSlot.appointmentStartTime}
                  disabled={
                    (timeSlot.statusOfTheBooking === 'PENDING' ||
                      timeSlot.statusOfTheBooking === 'ACCEPTED') &&
                    true
                  }
                  onPress={() => onSlotSelect(timeSlot.appointmentStartTime)}
                  type={
                    selectedSlot === timeSlot.appointmentStartTime
                      ? 'solid'
                      : 'outline'
                  }
                />
              </View>
            );
          })
        ) : (
          <Text>No Time Slots Available</Text>
        )}
      </View>
    </View>
  );
};
const getButtonStyle = (timeSlot, selectedSlot) => {
  if (selectedSlot === timeSlot.appointmentStartTime) {
    return styles.buttonSelected;
  } else if (timeSlot.statusOfTheBooking === 'PENDING') {
    return styles.buttonPending;
  } else {
    return styles.button;
  }
};

const styles = StyleSheet.create({
  mainBoxStyle: {
    paddingTop: 15,
  },
  timeParentBoxStyle: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 10,
    // alignItems: 'center',
  },
  button: {
    width: 80,
    height: 30,
    margin: '2%',
    alignItems: 'center',
    borderColor: '#00cc66',
    backgroundColor: 'white',
  },
  buttonPending: {
    width: 80,
    height: 30,
    margin: '2%',
    alignItems: 'center',
    borderColor: '#ffff99',
    backgroundColor: '#ffff99', //#ffff99
  },
  buttonSelected: {
    width: 80,
    height: 30,
    margin: '2%',
    alignItems: 'center',
    borderColor: '#00cc66',
    backgroundColor: '#00cc66',
  },
  titleStyle: {
    // textAlign: 'center',
    flex: 1,
    color: 'black',
    fontSize: 10,
    textAlignVertical: 'center',
  },
});

export default TimeSlotPicker;
