import React from 'react';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import {View, Text} from 'react-native';

const CustomDatePicker = ({date, onDateChange}) => {
  return (
    <View style={{padding: 10, paddingTop: 10}}>
      <Text style={{paddingBottom: 10}}>Select Appointment Date</Text>
      <DatePicker
        date={date}
        mode="date"
        placeholder="select date"
        format="MM-DD-YYYY"
        minDate={moment()
          .add(1, 'd')
          .format('MM-DD-YYYY')}
        maxDate={moment()
          .add(30, 'd')
          .format('MM-DD-YYYY')}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0,
          },
          dateInput: {
            marginLeft: 36,
          },
        }}
        onDateChange={onDateChange}
      />
    </View>
  );
};

export default CustomDatePicker;
