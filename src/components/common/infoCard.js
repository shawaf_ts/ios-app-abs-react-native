import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  convertTimeToReadableFormat,
  getWorkingDays,
  capitaliseFirstLetter,
} from '../../helpers/util';

const InfoCard = ({sellerInfo}) => {
  const startTime = convertTimeToReadableFormat(sellerInfo.startTime);
  const endTime = convertTimeToReadableFormat(sellerInfo.endTime);
  const breakStartTime = convertTimeToReadableFormat(sellerInfo.breakStartTime);
  const breakEndTime = convertTimeToReadableFormat(sellerInfo.breakEndTime);
  workingDaysString = getWorkingDays(sellerInfo.workingDays);
  console.log('INFOCARD:', sellerInfo);
  return (
    <View style={styles.mainView}>
      <View style={styles.nameView}>
        <Text style={styles.nameLabel}>Name:</Text>
        <Text style={styles.nameText}>
          {capitaliseFirstLetter(sellerInfo.name)}
        </Text>
      </View>
      <View style={styles.placeView}>
        <Text style={styles.placeLabel}>Place:</Text>
        <Text>{sellerInfo.place}</Text>
      </View>
      <View style={styles.availableView}>
        <Text style={styles.availableText}>Available:</Text>
        <View style={styles.availableContentView}>
          <Text style={{paddingVertical: 5}}>{workingDaysString}</Text>
          <Text style={{paddingVertical: 5}}>
            {`${startTime} - ${breakStartTime}`}
          </Text>
          <Text style={{paddingVertical: 5}}>
            {`${breakEndTime} - ${endTime}`}
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  nameView: {flex: 1, flexDirection: 'row', paddingVertical: 15},
  nameLabel: {width: '50%', alignSelf: 'center', fontSize: 20},
  placeView: {flex: 1, flexDirection: 'row', paddingVertical: 15},
  placeLabel: {width: '50%', alignSelf: 'center'},
  availableView: {flex: 1, flexDirection: 'row'},
  availableText: {width: '50%', alignSelf: 'center'},
  availableContentView: {width: '50%', flex: 1, flexDirection: 'column'},
  mainView: {
    padding: 10,
  },
  nameText: {
    fontSize: 20,
    paddingBottom: 10,
  },
});

export default InfoCard;
