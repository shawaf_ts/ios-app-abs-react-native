import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CustomCard from './customCard';
import {capitaliseFirstLetter} from '../helpers/util';
class CustomCards extends Component {
  state = {
    sellers: this.props.sellers,
  };

  async componentDidUpdate(prevProps) {
    if (this.props.sellers !== prevProps.sellers) {
      const sellers = this.props.sellers;
      this.setState({sellers});
    }
  }
  onCardPress = id => {
    console.log('Card Pressed', id);
    this.props.navigation.navigate('Booking', {sellerId: id});
  };

  render() {
    return (
      <View style={styles.cardContainer}>
        {0 !== this.state.sellers.length &&
          this.state.sellers.map(seller => {
            console.log('InsideMap, seller:', seller);
            return (
              <CustomCard
                key={seller._id}
                id={seller._id}
                title={capitaliseFirstLetter(seller.name)}
                place={seller.place}
                onCardPress={this.onCardPress}
              />
            );
          })}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  cardContainer: {alignItems: 'center', paddingTop: 10},
});

export default CustomCards;
