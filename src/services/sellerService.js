import http from './httpServices';
import {apiUrl} from '../config.json';

const apiEndPoint = apiUrl + 'sellers/';

export function getSellers() {
  return http.get(apiEndPoint);
}

export function getSeller(sellerId) {
  return http.get(apiEndPoint + sellerId);
}
