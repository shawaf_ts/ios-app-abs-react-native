import axios from 'axios';

axios.interceptors.response.use(null, error => {
  console.log('Logging the error:', error);
  if (!error.response) {
    console.log('Error.response not found');
    error.response = {status: 500};
  }

  return Promise.reject(error);
});

function setJwt(jwt) {
  axios.defaults.headers.common['x-auth-token'] = jwt;
}

export default {
  post: axios.post,
  get: axios.get,
  put: axios.put,
  delete: axios.delete,
  setJwt,
};
