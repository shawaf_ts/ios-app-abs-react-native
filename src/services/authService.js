import jwtDecode from 'jwt-decode';
import http from './httpServices';
import {apiUrl} from '../config.json';
import {AsyncStorage} from 'react-native';

const tokenKey = 'token';
const apiEndPoint = apiUrl + 'buyers/';

export async function login(email, password) {
  const {data: jwt} = await http.post(apiEndPoint + 'signin/', {
    email: email,
    password: password,
  });
  await AsyncStorage.setItem(tokenKey, jwt);
}

export async function checkLoggedIn() {
  try {
    const jwt = await AsyncStorage.getItem(tokenKey);
    if (!jwt) return false;
    return true;
  } catch (error) {
    return false;
  }
}

export async function signup(email, password, name) {
  const response = await http.post(apiEndPoint + 'signup/', {
    email: email,
    password: password,
    name: name,
  });
  await AsyncStorage.setItem(tokenKey, response.headers['x-auth-token']);
}

export async function getJwt() {
  return await AsyncStorage.getItem(tokenKey);
}

export async function logout() {
  await AsyncStorage.removeItem(tokenKey);
}

export async function getCurrentUser() {
  try {
    const jwt = await AsyncStorage.getItem(tokenKey);
    return jwtDecode(jwt);
  } catch (error) {
    return null;
  }
}
