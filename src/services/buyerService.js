import http from './httpServices';
import {apiUrl} from '../config.json';
import {getJwt} from './authService';

const apiEndPoint = apiUrl + 'buyers/';

export function getTimeSlots(sellerId, appointmentDate) {
  return http.get(
    apiEndPoint + 'get_slot_info/' + `${sellerId}/${appointmentDate}`,
  );
}

export async function bookAppointment(
  sellerId,
  buyerId,
  appointmentDate,
  timeSlot,
) {
  const token = await getJwt();
  //   console.log('Token:', token);
  http.setJwt(token);
  return http.post(apiEndPoint + 'make_appointment/', {
    sellerId: sellerId,
    buyerId: buyerId,
    appointmentDate: appointmentDate,
    appointmentStartTime: timeSlot,
  });
}

export function getAppointments(buyerId) {
  return http.get(apiEndPoint + 'get_appointments/' + buyerId);
}
