# Mobile Application for Appointment Booking System
* * *
Mobile application built using react native (version .61) for booking appointments.

## Features
* * *
* Register User
* User Login
* Search Sellers
* Select Sellers
* Book Appointments with Sellers
    * Select Date
    * Select Time Slots
    * Request Appointment
* View Appointment Request Status

## Screens
* * *
### 1. Register User
Allows user to register.

[ScreenShot](https://ibb.co/S71gxxt)
### 2. Login User
Allows user to login.

[ScreenShot](https://ibb.co/12Jrv45)
### 3. Home
Home Screen Contains a tab navigator with 3 screeens.
#### 3.1 Sellers
You can search or select sellers from this screen.

[ScreenShot](https://ibb.co/JjKqHW7)
#### 3.2 Appointments
This screen shows the appointments and status of the appointments, whether your appointment requests got accepted, rejected or still pending.

[ScreenShot](https://ibb.co/7SY8GC4)
### 4. Booking
You can make appointments request from this screen. This screen lets you choose available date and time for your appointments.

[ScreenShot](https://ibb.co/Y74d4PH)

## Deployment Guide
* * *
This guide provides step by step instructions to deploy this react native app in Xcode. Since we are using Xcode, the following guide only work on MacOS.

Since I have used node verision v10.19.0 for this setup, we need to create a virtual environment with this particular node version. We can achieve this with the help of nodeenv tool. 

Install nodeenv with the following command

`pip install nodeenv`

Now, create a directory where the code should reside, For example,

`mkdir devSetup; cd devSetup`

Create a node virtual environment with node version 10.19.0

`nodeenv --node=10.19.0 env`

Activate the virtual environment by running the following command.

`. env/bin/activate`

Check the version of node to verify virtual environment. Node version should be v10.19.0

`node --version`

Install watchman

`brew install watchman`

Install Xcode from App Store, if you already have installed xCode, ignore this step


You will also need to install the Xcode Command Line Tools. Open Xcode, then choose "Preferences..." from the Xcode menu. Go to the Locations panel and install the tools by selecting the most recent version in the Command Line Tools dropdown.


Once you have Xcode installed, install a simulator, open Xcode > Preferences... and select the Components tab. Select the latest version of iOS (13.2) to install.


Install cocoapods

`sudo gem install cocoapods`

Now, create react native application skeleton using the following command

`npx react-native init AppointmentBookingApp`

Once the project directory is created, cd into project directory. 

`cd AppointmentBookingApp`

Now copy the source directory and package.json from cloned repository to project directory.

`cp -r <path to Cloned Repo>/src .`
`cp <path to Cloned Repo/package.json .`

Remove the boiler plate App.js created by react-native-init

`rm App.js`

Now edit the index.js file to Resolve the App.js import. Open the App.js file, change

`import App from './App;'`   to `import App from './src/App';`

Install all the dependecies required for this project using the command   

`yarn install`

Install pods. Use the command below,

`cd ios/; pod install; cd -`

Now replace Info.plist file inside ios/\<Project Nname\> with the one provided in this repo

`cp <path to Cloned Repo>/Info.plist ios/AppointmentBooking/Info.plist`

Thats it, Now we can run the app in iOS simulutor using the command below

`npx react-native run-ios --configuration=release`
